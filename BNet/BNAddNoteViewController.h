//
//  MyTripNoteViewController.h
//  SmartTravel
//
//  Created by anton chudov on 07.06.15.
//  Copyright (c) 2015 AbyLab.com. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface BNAddNoteViewController : UIViewController <UITextViewDelegate>{

}


@property (weak, nonatomic) IBOutlet UITextView *noteText;

- (IBAction)buttonOK:(id)sender;
- (IBAction)buttonCancel:(id)sender;

- (IBAction)buttonDone:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *buttonDone;

@end
