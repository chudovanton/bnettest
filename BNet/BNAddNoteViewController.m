//
//  MyTripNoteViewController.m
//  SmartTravel
//
//  Created by anton chudov on 07.06.15.
//  Copyright (c) 2015 AbyLab.com. All rights reserved.
//

#import "BNAddNoteViewController.h"
#import "BNDataUpdater.h"



@interface BNAddNoteViewController ()

@end

@implementation BNAddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    

    _noteText.delegate  = self;
    _buttonDone.hidden  = true;
    
    
 }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)buttonDone:(id)sender {
    [_noteText resignFirstResponder];
    
    self.buttonDone.hidden = true;
}

- (IBAction)buttonOK:(id)sender{
    
    [[BNDataUpdater getInstance] addNote: self.noteText.text];

    [self.navigationController popViewControllerAnimated:true];

}

- (IBAction)buttonCancel:(id)sender{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)textViewDidChange:(UITextView *)textView{
    [textView scrollRangeToVisible:NSMakeRange(textView.text.length-1, 1)];
}

-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.buttonDone.hidden = false;
    
}
@end
