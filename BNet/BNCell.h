//
//  BNCell.h
//  BNet
//
//  Created by Chudov Anton on 03/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *createdDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *modifiedDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *itemTextView;

@end
