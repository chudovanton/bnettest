//
//  BNCell.m
//  BNet
//
//  Created by Chudov Anton on 03/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "BNCell.h"

@implementation BNCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
