//
//  DataUpdater.h
//  BNet
//
//  Created by Chudov Anton on 03/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BNItem.h"


#define BN_Token @"oESnVXS-4B-RQTgIrR"
#define BN_APIURL @"https://bnet.i-partner.ru/testAPI/"

@interface BNDataUpdater : NSObject

+ (BNDataUpdater*) getInstance;

+ (NSMutableURLRequest *) createPostRequest: (NSDictionary *) params;


- (void) addNote: (NSString *) noteText;
- (void) updateData;

@property (readonly, strong) NSMutableArray <BNItem *> * items;
@end
