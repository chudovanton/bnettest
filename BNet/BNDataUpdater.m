//
//  DataUpdater.m
//  BNet
//
//  Created by Chudov Anton on 03/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "BNDataUpdater.h"



@implementation BNDataUpdater{
    NSString * sessionId;
    NSString * token;
}

static BNDataUpdater* _instance = nil;

+(BNDataUpdater *)getInstance{
    if(_instance == nil){
        _instance = [[BNDataUpdater alloc] init];
        
    }
    
    return _instance;
}

- (instancetype) init{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if(![userDefaults stringForKey:@"bn_session"]){
        
        
        NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @"new_session",@"a",
                                 nil];
        
        NSMutableURLRequest * request = [BNDataUpdater createPostRequest:(NSDictionary *)params];
        
        
        
        NSURLSession * session = [NSURLSession sharedSession];
        NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
            if(data != nil){
                NSError *error = nil;
                NSDictionary *resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                
                if(error != nil){
                    return;
                }
                
                NSDictionary *dataValue = resp[@"data"];
                sessionId = dataValue[@"session"];
                
                [userDefaults setValue:sessionId forKey:@"bn_session"];
                
                [self updateData];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"NavigationStart" object:self];
            }
        }];
        
        
        [task resume];
    }
    else{
        sessionId = [userDefaults stringForKey:@"bn_session"];
        [self updateData];
    }
    return self;
}

- (void) addNote: (NSString*) noteText{
    
    NSString *text =  [noteText stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"add_entry",@"a",
                             sessionId, @"session",
                             text, @"body",
                             nil];
    
    NSMutableURLRequest * request = [BNDataUpdater createPostRequest:(NSDictionary *)params];
    
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataError" object:self];
        }
        else{
            [self addNoteResponse:data];
        }
        
        }];
    
    
    [task resume];

    
    
}
- (void) addNoteResponse: (NSData*) data{
    bool ok = false;
    
    if(data != nil){
        
        NSError * _Nullable error;
        
        NSDictionary *resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if(error == nil){
            if([(NSNumber*)resp[@"status"] intValue] == 1){
                ok = true;
                [self updateData];
            }
        }
        
    }
    if(!ok){
         [[NSNotificationCenter defaultCenter] postNotificationName:@"DataError" object:self];
    }
    
}


- (void) updateData {
    NSDictionary * params = [NSDictionary dictionaryWithObjectsAndKeys:
                             @"get_entries",@"a",
                             sessionId, @"session",
                             nil];
    
    NSMutableURLRequest * request = [BNDataUpdater createPostRequest:(NSDictionary *)params];
    
    
    
    NSURLSession * session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if(error){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DataError" object:self];
        }
        else{
            [self updateDataResponse:data];
        }
    }];
    
    
    [task resume];
    
}

- (void) updateDataResponse: (NSData *) data{
    NSError * _Nullable error;
    
    bool ok = false;
    
    if(data != nil){
        NSDictionary *resp = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        
        if(error == nil){
            if([(NSNumber*)resp[@"status"] intValue] == 1){
        
                NSArray *dataArray = resp[@"data"];
                _items = [[NSMutableArray alloc] init];
                
                dataArray = dataArray[0];
                for(int i = 0;i<dataArray.count;i++){
                    NSDictionary *dataItem = dataArray[i];
                    BNItem *item = [[BNItem alloc] initWithDictionary:dataItem];
                    
                    [_items addObject:item];
                    
                }
                
                ok = true;
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"DataUpdate" object:self];
            }
        }
    }
    if(!ok){
         [[NSNotificationCenter defaultCenter] postNotificationName:@"DataError" object:self];        
    }
    
}
+ (NSMutableURLRequest *) createPostRequest: (NSDictionary *) params{

    NSURL *url = [NSURL URLWithString: BN_APIURL];
    
    NSMutableURLRequest *request = [NSMutableURLRequest  requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue: BN_Token forHTTPHeaderField:@"token"];

    NSString *requestBody;
    
    NSEnumerator * enumerator = [params keyEnumerator];
    NSString *paramKey;
    
    while(paramKey = [enumerator nextObject]){
        NSString *paramValue = [params objectForKey:paramKey];
        
        NSString *pair = [NSString stringWithFormat:@"%@=%@", paramKey, paramValue];
        if(requestBody == nil) {
            
            requestBody = [NSString stringWithString:pair];
        }
        else {
            requestBody = [NSString stringWithFormat:@"%@&%@", requestBody, pair];
        }
    
    }


    [request setHTTPBody:[requestBody dataUsingEncoding:NSUTF8StringEncoding]];
    
    return request;
}

@end
