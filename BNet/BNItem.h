//
//  BNItem.h
//  BNet
//
//  Created by Chudov Anton on 05/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BNItem : NSObject

@property (strong, readonly) NSString *itemId;

@property (strong, readonly) NSString *itemText;

@property (strong, readonly) NSString * createdDate;
@property (strong, readonly) NSString * modifiedDate;




- (instancetype) initWithDictionary: (NSDictionary *) itemData;
@end
