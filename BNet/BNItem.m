//
//  BNItem.m
//  BNet
//
//  Created by Chudov Anton on 05/10/2016.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "BNItem.h"

@implementation BNItem {
  
    
   
}

-(instancetype)initWithDictionary:(NSDictionary *)itemData{
    _itemId   = itemData[@"id"];
    _itemText = itemData[@"body"];
    
    double dm = [(NSNumber *)itemData[@"dm"] doubleValue];
    double da  = [(NSNumber *)itemData[@"da"] doubleValue];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    _createdDate  = [dateFormatter stringFromDate: [NSDate dateWithTimeIntervalSince1970:da]];
    _modifiedDate = [dateFormatter stringFromDate: [NSDate dateWithTimeIntervalSince1970:dm]];
    
    
    return self;
}

@end
