//
//  MyTripNotesViewController.h
//  SmartTravel
//
//  Created by anton chudov on 06.06.15.
//  Copyright (c) 2015 AbyLab.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNNotesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;

- (IBAction)updateDataRequest:(id)sender;

@end
