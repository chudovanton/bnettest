//
//  RTNotesViewController.m
//  SmartTravel
//
//  Created by anton chudov on 06.06.15.
//  Copyright (c) 2015 AbyLab.com. All rights reserved.
//

#import "BNNotesViewController.h"
#import "BNAddNoteViewController.h"
#import "BNViewNoteViewController.h"

#import "BNDataUpdater.h"
#import "BNItem.h"
#import "BNCell.h"



@interface BNNotesViewController ()

@end

@implementation BNNotesViewController{
    NSArray <BNItem *> *items;
    BNDataUpdater *updater;

    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    items = [[NSArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataDidUpdate:) name:@"DataUpdate" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataUpdateError:) name:@"DataError" object:nil];

    
    updater = [BNDataUpdater getInstance];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(items == nil) return 0;
    return items.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BNCell *cell = [tableView dequeueReusableCellWithIdentifier: @"BNCell" forIndexPath:indexPath];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


- (void)configureCell:(BNCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    BNItem *item = items[indexPath.item];
    
    cell.createdDateLabel.text = [NSString stringWithFormat:@"Created %@",  item.createdDate];
    
    if([item.createdDate isEqual:item.modifiedDate]){
        cell.modifiedDateLabel.hidden = true;
    }
    else{
        cell.modifiedDateLabel.hidden = false;
        cell.modifiedDateLabel.text = [NSString stringWithFormat:@"Modified %@",  item.modifiedDate];
    }
    cell.itemTextView.text = item.itemText;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showItem"]) {
        
        BNViewNoteViewController * noteViewController= (BNViewNoteViewController*)segue.destinationViewController;
        NSIndexPath     *indexPath = [self.tableView indexPathForSelectedRow];
        BNItem    *item = items[indexPath.item];
        noteViewController.noteText = item.itemText;
    
    }
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated{
    [super setEditing:editing animated:animated];
    [self.tableView setEditing:editing];
}


-(void)dataDidUpdate: (NSNotification*) notification{
    
    items = updater.items;
    dispatch_async(dispatch_get_main_queue(),^{
        [self.tableView reloadData];
        
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        
        UIAlertController *alert;
        
        alert = [UIAlertController alertControllerWithTitle: @"Data update" message:@"Data was success updated" preferredStyle: UIAlertControllerStyleAlert];
        
        [alert addAction:defaultAction];
        
        alert.popoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alert animated:YES completion:nil];
        

    });
}

-(void)dataUpdateError: (NSNotification*) notification{
    
        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {}];
        
        UIAlertController *alert;
        
        alert = [UIAlertController alertControllerWithTitle: @"Data update" message:@"Data update error" preferredStyle: UIAlertControllerStyleAlert];
        
        [alert addAction:defaultAction];
        
        alert.popoverPresentationController.sourceView = self.view;
        
        [self presentViewController:alert animated:YES completion:nil];
    
}


- (IBAction)updateDataRequest:(id)sender {
    [updater updateData];
}
@end
