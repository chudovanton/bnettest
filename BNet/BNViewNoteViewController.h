//
//  BNViewNoteViewController.h
//  BNet
//
//  Created by Chudov Anton on 04/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BNViewNoteViewController : UIViewController

@property (weak, nonatomic) IBOutlet    UITextView * noteTextView;
@property (strong, nonatomic) NSString *noteText;

@end
