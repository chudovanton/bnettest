//
//  BNViewNoteViewController.m
//  BNet
//
//  Created by Chudov Anton on 04/10/16.
//  Copyright © 2016 Chudov Anton. All rights reserved.
//

#import "BNViewNoteViewController.h"

@interface BNViewNoteViewController ()

@end

@implementation BNViewNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.noteTextView.text = _noteText;
}

- (void)viewWillAppear:(BOOL)animated{
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

@end
